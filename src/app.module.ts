import { Module, ValidationPipe } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_GUARD, APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';

//App
import { AppController } from './app.controller';
import { AppService } from './app.service';

//Interceptor
import ResponseInterceptor from '@Application/interceptors/response.interceptor';

//Business Modules
import { AuthModule } from '@Modules/auth/auth.module';
import { CompanyModule } from '@Modules/company/company.module';
import { PermissionModule } from '@Modules/permission/permission.module';
import { RoleModule } from '@Modules/role/role.module';
import { UserModule } from '@Modules/user/user.module';

//Infra Modules
import { LambdaModule } from '@Infrastructure/lambda/lambda.module';
import { RedisModule } from '@Infrastructure/redis/redis.module';

//Guards
import { AuthJWTGuard } from '@Application/guards/auth.guard';
import { CompanyGuard } from '@Application/guards/company.guard';
import { OnlyMeGuard } from '@Application/guards/onlyMe.guard';
import { PermissionGuard } from '@Application/guards/permission.guard';
import { RoleGuard } from '@Application/guards/role.guard';
import { CompanyFilterGuard } from './application/filters/company.filter';
import { BuildQuery } from './application/pipes/build-query.pipe';
import { ValidateBody } from './application/validation/body.validation';
import { PrismaModule } from './infrastructure/prisma/prisma.module';
import { UtilityModule } from './infrastructure/utilities/utilities.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true,
        }),
        //JWT
        JwtModule.registerAsync({
            useFactory: async (configService: ConfigService) => ({
                secret: process.env.JWT_SECRET,
                signOptions: { expiresIn: '12h' },
            }),
            inject: [ConfigService],
        }),
        //Infra Modules
        LambdaModule,
        PrismaModule,
        RedisModule,
        UtilityModule,
        //Business Module
        AuthModule,
        UserModule,
        CompanyModule,
        RoleModule,
        PermissionModule,
    ],
    controllers: [AppController],
    providers: [
        {
            provide: APP_INTERCEPTOR,
            useClass: ResponseInterceptor,
        },
        {
            provide: APP_GUARD,
            useClass: AuthJWTGuard,
        },
        {
            provide: APP_GUARD,
            useClass: PermissionGuard,
        },
        {
            provide: APP_GUARD,
            useClass: CompanyGuard,
        },
        {
            provide: APP_GUARD,
            useClass: RoleGuard,
        },
        {
            provide: APP_GUARD,
            useClass: OnlyMeGuard,
        },
        {
            provide: APP_GUARD,
            useClass: CompanyFilterGuard,
        },
        {
            provide: APP_PIPE,
            useClass: ValidateBody,
        },
        {
            provide: APP_PIPE,
            useClass: ValidationPipe,
        },
        {
            provide: APP_PIPE,
            useClass: BuildQuery,
        },
        AppService,
    ],
})
export class AppModule {}
