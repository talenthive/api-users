import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthModule } from './modules/auth/auth.module';
import { UserModule } from './modules/user/user.module';
import { PrismaModule } from './infrastructure/prisma/prisma.module';
import { LambdaModule } from './infrastructure/lambda/lambda.module';
import { RedisModule } from './infrastructure/redis/redis.module';
import { JwtModule } from '@nestjs/jwt';
import { APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { AuthJWTGuard } from './application/guards/auth.guard';
import { PermissionGuard } from './application/guards/permission.guard';
import { PermissionModule } from './modules/permission/permission.module';
import { PassportModule } from '@nestjs/passport';
import { RedisService } from './infrastructure/redis/redis.service';

const mockRedisClient = {
    get: jest.fn(),
    set: jest.fn(),
    del: jest.fn(),
    disconnect: jest.fn(),
};

@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true,
        }),
        //JWT
        JwtModule.registerAsync({
            useFactory: async (configService: ConfigService) => ({
                secret: configService.get('JWT_SECRET'),
                signOptions: { expiresIn: '12h' },
            }),
            inject: [ConfigService],
        }),
        PassportModule,
        PrismaModule,
        AuthModule,
        UserModule,
        LambdaModule,
        PermissionModule,
        RedisModule,
    ],
    controllers: [],
    providers: [
        {
            provide: APP_GUARD,
            useClass: AuthJWTGuard,
        },
        {
            provide: APP_GUARD,
            useClass: PermissionGuard,
        },
    ],
})
export class TestModule {}
