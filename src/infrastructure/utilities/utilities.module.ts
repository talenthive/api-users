import { Global, Module } from '@nestjs/common';

//Service
import { UtilityService } from './utilities.service';

//Modules
@Global()
@Module({
    imports: [],
    exports: [UtilityService],
    controllers: [],
    providers: [UtilityService],
})
export class UtilityModule {}
