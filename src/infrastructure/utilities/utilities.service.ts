import { Injectable } from '@nestjs/common';
import { RedisService } from '../redis/redis.service';

@Injectable()
export class UtilityService {
    constructor(protected readonly redisService?: RedisService) {}

    async generatePublicId(name: string, entity: string = ''): Promise<string> {
        const transformedName = name
            .toLowerCase()
            .trim()
            .replace(/[^a-z0-9\s-]/g, '') // Remove non-alphanumeric characters except spaces and hyphens
            .replace(/\s+/g, '-') // Replace spaces with hyphens
            .replace(/-+/g, '-') // Replace multiple hyphens with a single hyphen
            .replace(/^-+|-+$/g, ''); // Trim leading/trailing hyphens

        // Increment the value in Redis
        const publicId = (
            await this.redisService.increment('publicId', entity)
        ).toString();

        return `${transformedName}-${publicId}`;
    }
}
