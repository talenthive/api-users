// ESM
import { faker } from '@faker-js/faker';

// Entity
import { $Enums, Prisma } from '@Types';

// DTO
import { PermissionUpdateList } from '@/modules/permission/dto/permission.update.input';
import { PermissionCreateInput } from '@Modules/permission/dto/permission.create.input';

//Prisma Connect
import prisma from '@Prisma/prisma.connect';

export const createFakePermissionActions = (): PermissionUpdateList => {
    const newPermission = {
        entity: faker.helpers.enumValue($Enums.Entity),
        action: faker.helpers.enumValue($Enums.PermissionAction),
    };
    return newPermission;
};

export const createFakePermission =
    async (): Promise<PermissionCreateInput> => {
        const apps = await prisma.app.findMany();
        const roles = await prisma.role.findMany();
        const companies = await prisma.company.findMany();
        const users = await prisma.user.findMany();

        const random = faker.number.int({ min: 1, max: 3 });

        if (apps.length === 0) {
            throw new Error('No app found');
        }

        const newPermission = {
            entity: faker.helpers.enumValue($Enums.Entity),
            action: faker.helpers.enumValue($Enums.PermissionAction),
            appId: faker.helpers.arrayElement(apps)?.id,
            roleId:
                random === 1 && roles.length > 0
                    ? faker.helpers.arrayElement(roles)?.id
                    : null,
            userId:
                random === 2 && users.length > 0
                    ? faker.helpers.arrayElement(users)?.id
                    : null,
            companyId:
                random === 3 && companies.length > 0
                    ? faker.helpers.arrayElement(companies)?.id
                    : null,
        };

        return newPermission;
    };

export const generateFakePermissions = async (
    count = 5
): Promise<Prisma.PermissionCreateManyInput[]> => {
    try {
        const promises = Array.from({ length: count }, () =>
            createFakePermission()
        );
        const permissions = await Promise.all(promises);
        return permissions;
    } finally {
        await prisma.$disconnect();
    }
};

export const generateFakePermissionActions = async (
    count = 15
): Promise<PermissionUpdateList[]> => {
    try {
        const promises = Array.from({ length: count }, () =>
            createFakePermissionActions()
        );
        const permissions = await Promise.all(promises);
        return permissions;
    } finally {
        await prisma.$disconnect();
    }
};
