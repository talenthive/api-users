// ESM
import { faker } from '@faker-js/faker';

//Entity
import { Prisma } from '@Types';

//DTO
import { RoleCreateInput } from '@Modules/role/dto/role.create.input';

//Fucntion
import { generateRandomString } from '@Functions/generate-public-id.function';

export const createFakeRole = (roleNames = []): RoleCreateInput => {
    let element: RoleCreateInput;
    if (roleNames.length > 0) {
        element = {
            name: roleNames[0],
            publicId: generateRandomString(),
        };
        roleNames.shift();
    } else {
        const roleName = faker.person.jobType();
        element = {
            name: roleName,
            publicId: generateRandomString(),
        };
    }

    return element;
};

export const generateFakeRoles = (count = 5): Prisma.RoleCreateManyInput[] => {
    const roleNames = faker.helpers.uniqueArray(faker.person.jobType, count);
    return faker.helpers.multiple(() => createFakeRole(roleNames), {
        count,
    });
};
