// ESM
import { faker } from '@faker-js/faker';

//Entity
import { Prisma } from '@Types';

//DTO
import { CompanyCreateInput } from '@Modules/company/dto/company.create.input';

//Fucntion
import { generateRandomString } from '@Functions/generate-public-id.function';

export const createFakeCompany = (): CompanyCreateInput => {
    const companyName = faker.company.name();
    return {
        name: companyName,
        publicId: generateRandomString(),
    };
};

export const generateFakeCompanies = (
    count = 5
): Prisma.CompanyCreateManyInput[] => {
    return faker.helpers.multiple(createFakeCompany, {
        count,
    });
};
