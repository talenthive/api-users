// ESM
import { faker } from '@faker-js/faker';

//Entity
import { Company, Prisma, Role } from '@Types';

//Prisma Connect
import prisma from '@/infrastructure/prisma/prisma.connect';

export const createFakeUser = (
    roles: Role[] = [],
    companies: Company[] = []
): Prisma.UserCreateInput => {
    const nbCompanies = faker.number.int({ min: 1, max: 3 });
    const nbRoles = faker.number.int({ min: 1, max: 3 });

    const chosenRoles: Role[] = faker.helpers.arrayElements(roles, nbRoles);
    const chosenCompanies: Company[] = faker.helpers.arrayElements(
        companies,
        nbCompanies
    );

    return {
        email: faker.internet.email(),
        firstname: faker.internet.userName(),
        lastname: faker.internet.userName(),
        password: faker.internet.password(),
        companies: {
            connect: chosenCompanies,
        },
        roles: {
            connect: chosenRoles,
        },
    };
};

export const generateFakeUsers = async (
    count = 5
): Promise<Prisma.UserCreateManyInput[]> => {
    const roles = await prisma.role.findMany({});
    const companies = await prisma.company.findMany();
    const users = faker.helpers.multiple(
        () => createFakeUser(roles, companies),
        {
            count,
        }
    );

    return users;
};
