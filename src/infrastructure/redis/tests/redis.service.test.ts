import { RedisService } from '../redis.service';
import Redis from 'ioredis';

describe('RedisService', () => {
    let redisService: RedisService;

    beforeAll(() => {
        redisService = new RedisService(new Redis());
    });

    afterAll(async () => {
        await redisService.onModuleDestroy();
    });

    test('Service Should be defined', async () => {
        expect(redisService).toBeDefined();
    });

    test('Should set and get a value from Redis', async () => {
        await redisService.set('prefix', 'test_key', 'test_value');
        const value = await redisService.get('prefix', 'test_key');
        expect(value).toBe('test_value');
    });

    test('Should delete a value from Redis', async () => {
        await redisService.set('prefix', 'test_key', 'test_value');
        await redisService.delete('prefix', 'test_key');
        const value = await redisService.get('prefix', 'test_key');
        expect(value).toBeNull();
    });
});
