import { Injectable } from '@nestjs/common';

//Interfaces
import { ApiResponse } from '@Application/interfaces/api.response.interface';
import { ApiRequest } from '@/application/interfaces/api.request.interface';

@Injectable()
export class LambdaService {
    async invokeApi(params: ApiRequest): Promise<ApiResponse> {
        try {
            const { method, url, payload } = params;

            const response = await fetch(url, {
                method,
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(payload),
            });
            if (!response.ok) {
                const errorData = await response.json();
                throw new Error(`Error: ${errorData.message}`);
            }
            return response.json();
        } catch (error) {
            throw new Error(`Error invoking API Rest: ${error}`);
        }
    }
}
