import { Global, Module } from '@nestjs/common';

//Service
import { LambdaService } from './lambda.service';

//Modules
@Global()
@Module({
    imports: [],
    exports: [LambdaService],
    controllers: [],
    providers: [LambdaService],
})
export class LambdaModule {}
