-- AlterTable
ALTER TABLE "Permission" ADD COLUMN     "active" BOOLEAN NOT NULL DEFAULT true;

-- AlterTable
ALTER TABLE "Role" ALTER COLUMN "publicId" DROP NOT NULL;
