/*
  Warnings:

  - Changed the type of `entity` on the `Permission` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.

*/
-- CreateEnum
CREATE TYPE "Entity" AS ENUM ('COMPANY', 'USER');

-- AlterTable
ALTER TABLE "Permission" DROP COLUMN "entity",
ADD COLUMN     "entity" "Entity" NOT NULL;
