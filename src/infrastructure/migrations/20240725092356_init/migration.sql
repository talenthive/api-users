/*
  Warnings:

  - Added the required column `action` to the `Permission` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "PermissionAction" AS ENUM ('CREATE', 'READ', 'UPDATE', 'DELETE');

-- CreateEnum
CREATE TYPE "PermissionOrigin" AS ENUM ('COMPANY', 'ROLE', 'NONE');

-- AlterTable
ALTER TABLE "Permission" ADD COLUMN     "action" "PermissionAction" NOT NULL;
