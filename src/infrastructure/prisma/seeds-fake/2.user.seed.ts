import { generateFakeUsers } from '@/infrastructure/faker/user.faker';
import { Prisma } from '@Types';

//Prisma Connect
import prisma from '@Prisma/prisma.connect';

async function main() {
    const users: Prisma.UserCreateInput[] = await generateFakeUsers();

    console.log('Add Users: ', users);

    for (const user of users) {
        await prisma.user.create({ data: user }); // Assuming createUser is the method to add a user in your service
    }
}

main()
    .catch((e) => {
        console.error(e);
        process.exit(1);
    })
    .finally(async () => {
        await prisma.$disconnect();
    });
