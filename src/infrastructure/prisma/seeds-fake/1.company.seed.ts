import { generateFakeCompanies } from '@/infrastructure/faker/company.faker';
import { Prisma } from '@Types';

//Prisma Connect
import prisma from '@Prisma/prisma.connect';

async function main() {
    const companies: Prisma.CompanyCreateInput[] = generateFakeCompanies();

    const addCompanies = async () =>
        await prisma.company.createMany({ data: companies });

    console.log('Add Companies: ', companies);

    addCompanies();
}

main()
    .catch((e) => {
        console.error(e);
        process.exit(1);
    })
    .finally(async () => {
        await prisma.$disconnect();
    });
