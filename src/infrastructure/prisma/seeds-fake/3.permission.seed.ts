import { generateFakePermissions } from '@/infrastructure/faker/permission.faker';
import { Prisma } from '@Types';

//Prisma Connect
import prisma from '@Prisma/prisma.connect';

async function main() {
    const permissions: Prisma.PermissionCreateManyInput[] =
        await generateFakePermissions(25);

    console.log('Add Permissions: ', permissions);

    const addPermissions = async () =>
        await prisma.permission.createMany({ data: permissions });

    addPermissions();
}

main()
    .catch((e) => {
        console.error(e);
        process.exit(1);
    })
    .finally(async () => {
        await prisma.$disconnect();
    });
