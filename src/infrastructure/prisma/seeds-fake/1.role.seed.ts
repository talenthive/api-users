import { generateFakeRoles } from '@/infrastructure/faker/role.faker';
import { Prisma } from '@Types';

//Prisma Connect
import prisma from '@Prisma/prisma.connect';

async function main() {
    const roles: Prisma.RoleCreateInput[] = generateFakeRoles();

    const addRoles = async () => await prisma.role.createMany({ data: roles });

    console.log('Add Roles: ', roles);

    addRoles();
}

main()
    .catch((e) => {
        console.error(e);
        process.exit(1);
    })
    .finally(async () => {
        await prisma.$disconnect();
    });
