const { execSync } = require('child_process');
const path = require('path');
const fs = require('fs');

// Change this to your TypeScript files directory

const folder = process.argv[2];

// Get the specific file to run from the command line arguments
const specificFile = process.argv[3];

//Feed Data Process
const feedData = (folder) => {
    const dir = path.resolve(__dirname, `seeds-${folder}`);

    fs.readdirSync(dir).forEach((file) => {
        if (file.endsWith('.ts')) {
            if (!specificFile || file === specificFile) {
                const filePath = path.join(dir, file);
                console.log(`Running ${filePath}`);
                // Add `-r tsconfig-paths/register` to the command
                execSync(`ts-node -r tsconfig-paths/register ${filePath}`, {
                    stdio: 'inherit',
                });
            }
        }
    });
};

//Fake Data
if (folder === 'fake') {
    console.log('Seeding fake data...');
    feedData(folder);
} else if (folder === 'init') {
    console.log('Seeding init data...');
    feedData(folder);
} else {
    console.log('Seeding all data...');
    feedData('init');
    feedData('fake');
}
