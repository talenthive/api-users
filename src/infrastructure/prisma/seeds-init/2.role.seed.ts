import { Prisma } from '@Types';

//Prisma Connect
import prisma from '@Prisma/prisma.connect';

async function main() {
    await prisma.role.deleteMany({});

    const roles: Prisma.RoleCreateInput[] = [];

    roles.push({
        name: 'Super Admin',
        publicId: 'super-admin',
    });

    const addRoles = async () => await prisma.role.createMany({ data: roles });

    console.log('Add Roles: ', roles);

    addRoles();
}

main()
    .catch((e) => {
        console.error(e);
        process.exit(1);
    })
    .finally(async () => {
        await prisma.$disconnect();
    });
