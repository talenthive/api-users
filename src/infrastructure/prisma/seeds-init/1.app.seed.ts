import { Prisma, PrismaClient } from '@Types';

//Prisma Connect
import prisma from '@Prisma/prisma.connect';

async function main() {
    const existingApps = await prisma.app.findMany();
    if (existingApps.length > 0) return;

    const apps: Prisma.AppCreateManyInput[] = [
        {
            name: 'App - Onboarding',
            publicId: 'on-boarding',
        },
    ];

    const addApps = async () => await prisma.app.createMany({ data: apps });

    console.log('Add Applications: ', apps);

    addApps();
}

main()
    .catch((e) => {
        console.error(e);
        process.exit(1);
    })
    .finally(async () => {
        await prisma.$disconnect();
    });
