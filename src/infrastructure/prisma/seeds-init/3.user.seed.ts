//Prisma Connect
import prisma from '@Prisma/prisma.connect';

async function main() {
    await prisma.user.deleteMany({});

    //Super admin role
    const role = await prisma.role.findUnique({
        where: {
            publicId: 'super-admin',
        },
    });

    //Add Admin
    const user = {
        email: 'root@root.com',
        firstname: 'root',
        lastname: 'root',
        password: 'root',
        roles: {
            connect: {
                id: role.id,
            },
        },
    };

    await prisma.user.create({ data: user });
}

main()
    .catch((e) => {
        console.error(e);
        process.exit(1);
    })
    .finally(async () => {
        await prisma.$disconnect();
    });
