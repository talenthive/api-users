import { Test } from '@nestjs/testing';
import { UserController } from '../user.controller';
import { UserService } from '../user.service';
import { TestModule } from '@/test.module';
import { PermissionService } from '@/modules/permission/permission.service';

describe('UserController', () => {
    let userController: UserController;

    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [TestModule],
            controllers: [UserController],
            providers: [UserService, PermissionService],
        }).compile();

        userController = moduleRef.get<UserController>(UserController);
    });

    it('User Controller Should be defined', () => {
        expect(userController).toBeDefined();
    });
});
