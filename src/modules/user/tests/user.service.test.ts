import { Test, TestingModule } from '@nestjs/testing';

import { UserService } from '../user.service';
import { createFakeUser } from '@/infrastructure/faker/user.faker';
import { UserCreateInput } from '../dto/user.create.input';
import { TestModule } from '@/test.module';

describe('User Service', () => {
    let userService: UserService;
    let user: UserCreateInput;
    let userId: number;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [TestModule],
            controllers: [],
            providers: [UserService],
        }).compile();

        userService = module.get<UserService>(UserService);
    });

    //Service
    it('User Service should be defined', () => {
        expect(userService).toBeDefined();
    });

    //Generate fake user data
    it('Should generate a fake user', async () => {
        user = createFakeUser();
        expect(user).toBeDefined();
        expect(Object.keys(user).length).toBeGreaterThan(0);
    });

    //Create User
    it('Should create a user', async () => {
        const newUser = await userService.create(user);
        userId = newUser.id;
        expect(Object.keys(newUser).length).toBeGreaterThan(0);
    });

    //Update User
    it('Should update a user', async () => {
        const fakeUser = createFakeUser();
        const updatedUser = { ...fakeUser, id: userId };
        const user = await userService.update(userId, updatedUser);
        expect(Object.keys(user).length).toBeGreaterThan(0);
    });

    //Find Users
    it('Should return users', async () => {
        const users = await userService.find({});
        expect(users?.length).toBeGreaterThan(0);
    });

    //Delete User
    it('Should delete the user', async () => {
        const result = await userService.delete(userId);
        expect(result).toBeTruthy();
    });
});
