import { CreateDTO } from '@/application/dtos/create.dto';
import { Prisma } from '@Types';
import {
    IsEmail,
    IsNotEmpty,
    IsOptional,
    IsString,
    IsStrongPassword,
    MinLength,
} from 'class-validator';

export class UserCreateInput
    extends CreateDTO
    implements Prisma.UserCreateInput
{
    @IsEmail()
    @IsNotEmpty()
    email: string;

    @IsOptional()
    @IsNotEmpty()
    @MinLength(8)
    @IsStrongPassword()
    password?: string;

    @IsString()
    firstname: string;

    @IsString()
    lastname: string;
}
