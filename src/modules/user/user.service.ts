import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcryptjs';

//Prisma
import { Company, Prisma, Role, User } from '@Types';

//Generic
import { PrismaService } from '@/infrastructure/prisma/prisma.service';
import { GenericService } from '@Application/services/generic.service';

//DTO
import { UserCreateInput } from './dto/user.create.input';

//Functions
import { generateRandomString } from '@Functions/generate-random-strings.function';
import { UserWithAllRelations } from './interfaces/user.all-relations.interface';

@Injectable()
export class UserService extends GenericService<User, Prisma.UserDelegate> {
    constructor(protected readonly prismaService: PrismaService) {
        super('user', prismaService);
    }

    //Find Users
    async find(params: Prisma.UserFindManyArgs): Promise<User[] | undefined> {
        const users = await this.repository.findMany(params);
        return users;
    }

    //Find User By Email
    async findOneByEmail(email: string): Promise<User> {
        return await this.repository.findUnique({
            where: { email },
        });
    }

    //Find User Roles
    async findUserRoles(userId: number): Promise<Role[]> {
        const user = await this.repository.findUnique({
            where: { id: userId },
            include: { roles: true },
        });
        return user.roles;
    }

    //Find Single User
    async findById(id: number): Promise<UserWithAllRelations> {
        const user = await this.repository.findUnique({
            where: {
                id,
            },
            include: {
                companies: true,
                roles: true,
            },
        });
        // Exclude the password from the user object
        if (user) user.password = null;
        return user;
    }

    //Add User
    async create(input: UserCreateInput): Promise<User> {
        //Generate Password if does not exist
        if (!input.password) {
            input.password = generateRandomString(24);
        }

        //Generate Password
        const newPassword: string = await new Promise((resolve, reject) => {
            bcrypt.hash(input.password, 10, (err, hash) => {
                if (err) {
                    reject('Error while hasing the password');
                }
                resolve(hash);
            });
        });

        //Gather the credentials
        const data = {
            email: input.email,
            password: newPassword,
            firstname: input.firstname,
            lastname: input.lastname,
        };

        //Create the user
        const newUser = await this.repository.create({ data });

        return newUser;
    }

    //Update User Password
    async updatePassword(
        id: number,
        password: string
    ): Promise<Boolean | Error> {
        //Check if password is different
        const user = await this.findById(id);
        const currentPassword = user.password;

        const checkPasswordIsSame = await new Promise((resolve, reject) => {
            bcrypt.compare(password, currentPassword, (err, result) => {
                if (err) {
                    reject();
                } else {
                    resolve(result);
                }
            });
        });
        if (checkPasswordIsSame) {
            throw new Error('Your password must not be the same');
        }
        //Generate Password
        const newPassword = await new Promise((resolve, reject) => {
            bcrypt.hash(password, 10, (err, hash) => {
                if (err) {
                    reject('Error while hasing the password');
                }
                resolve(hash);
            });
        });

        await this.update(id, { password: newPassword });
        return true;
    }
}
