import {
    HttpException,
    HttpStatus,
    Injectable,
    PipeTransform,
} from '@nestjs/common';
import { User } from '@Types';
import { UserService } from './user.service';

@Injectable()
export class HydrateUser implements PipeTransform {
    constructor(private readonly userService: UserService) {}

    async transform(value: number, metadata: any): Promise<User | null> {
        value = typeof value === 'string' ? (value = parseInt(value)) : value;
        const user = await this.userService.findById(value);
        if (!user) {
            throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
        }
        return user;
    }
}
