import { Module } from '@nestjs/common';

//Controller
import { UserController } from './user.controller';

//Service
import { UserService } from './user.service';
import { PermissionModule } from '../permission/permission.module';

//Modules

@Module({
    imports: [PermissionModule],
    exports: [UserService],
    controllers: [UserController],
    providers: [UserService],
})
export class UserModule {}
