import {
    Body,
    Controller,
    Get,
    HttpException,
    HttpStatus,
    Param,
    Patch,
    Post,
    Query,
    Req,
} from '@nestjs/common';

//Services
import { UserService } from '@Modules/user/user.service';
import { PermissionService } from '../permission/permission.service';

//DTO
import { UserCreateInput } from './dto/user.create.input';
import { UserUpdateInput } from './dto/user.update.input';

//Prisma
import { User } from '@Types';

//Interfaces
import { AppPermission } from '../permission/interfaces/permissions-by-user.interface';

//Guards
import { OnlyMe } from '@/application/guards/onlyMe.guard';
import { Permission } from '@/application/guards/permission.guard';

@Controller('user')
export class UserController {
    constructor(
        private readonly userService: UserService,
        private readonly permissionService: PermissionService
    ) {}

    //Add User
    @Post('/create')
    async create(@Body() body: UserCreateInput): Promise<User> {
        return await this.userService.create(body);
    }

    //Get Current User
    @Get('/current')
    async findCurrentUser(@Req() req): Promise<User> {
        const user = req?.user;
        if (!user)
            throw new HttpException(
                'Cannot find your profile',
                HttpStatus.NOT_FOUND
            );
        return user;
    }

    //Get User Permissions
    @Get('/current/permissions')
    async getUserPermissions(@Req() req): Promise<AppPermission[]> {
        const user = req?.user;
        return this.permissionService.findUserPermissions(user, req.appId);
    }

    //Update User
    @Patch('/:userId')
    @OnlyMe(true)
    async update(
        @Body()
        body: UserUpdateInput,
        @Param('userId')
        userId: number
    ): Promise<User> {
        const updatedUser = await this.userService.update(userId, body);
        return updatedUser;
    }

    //Update User Password
    @Patch('/password/:user_id')
    @OnlyMe(true)
    async updatePassword(
        @Body('password')
        password: string,
        @Param('userId')
        userId: number
    ): Promise<Boolean | Error> {
        return await this.userService.updatePassword(userId, password);
    }
}
