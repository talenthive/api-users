import { Company, Role, User } from '@Types';

export interface UserWithAllRelations extends User {
    companies: Array<Company>;
    roles: Array<Role>;
}
