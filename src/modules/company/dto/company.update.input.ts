import { PartialType } from '@nestjs/mapped-types';
import { CompanyCreateInput } from './company.create.input';

export class CompanyUpdateInput extends PartialType(CompanyCreateInput) {}
