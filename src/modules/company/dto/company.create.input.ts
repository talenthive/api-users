import { CreateDTO } from '@/application/dtos/create.dto';
import { Prisma } from '@Types';
import { Transform } from 'class-transformer';
import { IsNotEmpty, IsOptional, IsString, ValidateIf } from 'class-validator';

//Functions

export class CompanyCreateInput
    extends CreateDTO
    implements Prisma.CompanyUncheckedCreateInput
{
    @IsString()
    @IsNotEmpty()
    name: string;

    @IsString()
    @IsOptional()
    publicId: string;
}
