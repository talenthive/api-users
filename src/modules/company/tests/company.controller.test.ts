import { Test } from '@nestjs/testing';
import { createFakeCompany } from '@/infrastructure/faker/company.faker';
import { CompanyController } from '../company.controller';
import { CompanyService } from '../company.service';
import { TestModule } from '@/test.module';
import { CompanyCreateInput } from '../dto/company.create.input';

describe('CompanyController', () => {
    let companyController: CompanyController;
    let company: CompanyCreateInput;
    let companyId: number;

    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [TestModule],
            controllers: [CompanyController],
            providers: [CompanyService],
        }).compile();

        companyController = moduleRef.get<CompanyController>(CompanyController);
    });

    it('Company Controller Should be defined', () => {
        expect(companyController).toBeDefined();
    });

    //Generate fake company data
    it('Should generate a fake company', async () => {
        company = createFakeCompany();
        expect(company).toBeDefined();
        expect(Object.keys(company).length).toBeGreaterThan(0);
    });

    //Create Company
    it('Should create a company', async () => {
        const newCompany = await companyController.create(company);
        companyId = newCompany.id;
        expect(Object.keys(newCompany).length).toBeGreaterThan(0);
    });

    //Update Company
    it('Should update a company', async () => {
        const fakeCompany = createFakeCompany();
        const updatedCompany = { ...fakeCompany, id: companyId };
        const company = await companyController.update(
            companyId,
            updatedCompany
        );
        expect(Object.keys(company).length).toBeGreaterThan(0);
    });

    //Find Companies
    // it('Should return companies', async () => {
    //     const companys = await companyController.find({});
    //     expect(companys?.length).toBeGreaterThan(0);
    // });

    //Delete Company
    it('Should delete the company', async () => {
        const result = await companyController.delete(companyId);
        expect(result).toBeTruthy();
    });
});
