import { Test, TestingModule } from '@nestjs/testing';

import { createFakeCompany } from '@/infrastructure/faker/company.faker';
import { TestModule } from '@/test.module';
import { CompanyService } from '../company.service';
import { CompanyCreateInput } from '../dto/company.create.input';

describe('Company Service', () => {
    let companyService: CompanyService;
    let company: CompanyCreateInput;
    let companyId: number;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [TestModule],
            controllers: [],
            providers: [CompanyService],
        }).compile();

        companyService = module.get<CompanyService>(CompanyService);
    });

    //Service
    it('Company Service should be defined', () => {
        expect(companyService).toBeDefined();
    });

    //Generate fake company data
    it('Should generate a fake company', async () => {
        company = createFakeCompany();
        expect(company).toBeDefined();
        expect(Object.keys(company).length).toBeGreaterThan(0);
    });

    //Create Company
    it('Should create a company', async () => {
        const newCompany = await companyService.create(company);
        companyId = newCompany.id;
        expect(Object.keys(newCompany).length).toBeGreaterThan(0);
    });

    //Update Company
    it('Should update a company', async () => {
        const fakeCompany = createFakeCompany();
        const updatedCompany = { ...fakeCompany, id: companyId };
        const company = await companyService.update(companyId, updatedCompany);
        expect(Object.keys(company).length).toBeGreaterThan(0);
    });

    //Find Companys
    it('Should return companies', async () => {
        const companys = await companyService.find({});
        expect(companys?.length).toBeGreaterThan(0);
    });

    //Delete Company
    it('Should delete the company', async () => {
        const result = await companyService.delete(companyId);
        expect(result).toBeTruthy();
    });
});
