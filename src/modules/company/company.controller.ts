import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
} from '@nestjs/common';

//Services
import { CompanyService } from './company.service';

//DTO
import { CompanyCreateInput } from './dto/company.create.input';
import { CompanyUpdateInput } from './dto/company.update.input';

//Prisma
import { Company, Prisma } from '@Types';

//Pipes

//Guard
import { CompanyFilter } from '@/application/filters/company.filter';
import { CheckCompany } from '@/application/guards/company.guard';
import { Permission } from '@/application/guards/permission.guard';

@Controller('company')
export class CompanyController {
    constructor(private readonly companyService: CompanyService) {}

    //Create Company
    @Post('/create')
    @Permission('COMPANY', 'CREATE')
    async create(@Body() body: CompanyCreateInput): Promise<Company> {
        return await this.companyService.create(body);
    }

    //Get Companies
    @Get('/find')
    @Permission('COMPANY', 'READ')
    @CompanyFilter(true)
    async find(@Body() query: Prisma.CompanyAggregateArgs): Promise<Company[]> {
        return this.companyService.find(query);
    }

    //Get Company
    @Get('/find/:companyId')
    @Permission('COMPANY', 'READ')
    @CheckCompany(true)
    async findOne(
        @Param('companyId')
        companyId: number
    ): Promise<Company> {
        return this.companyService.findById(companyId);
    }

    //Update Company
    @Patch('/update/:companyId')
    @Permission('COMPANY', 'UPDATE')
    @CheckCompany(true)
    async update(
        @Param('companyId')
        companyId: number,
        @Body()
        body: CompanyUpdateInput
    ): Promise<Company> {
        return await this.companyService.update(companyId, body);
    }

    //Delete Company
    @Delete('/delete/:companyId')
    @Permission('COMPANY', 'DELETE')
    @CheckCompany(true)
    async delete(
        @Param('companyId')
        companyId: number
    ): Promise<Boolean> {
        return this.companyService.delete(companyId);
    }
}
