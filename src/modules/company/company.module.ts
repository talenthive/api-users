import { Module } from '@nestjs/common';

//Controller
import { CompanyController } from './company.controller';

//Service
import { CompanyService } from './company.service';

@Module({
    exports: [CompanyService],
    controllers: [CompanyController],
    providers: [CompanyService],
})
export class CompanyModule {}
