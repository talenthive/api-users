import { Injectable } from '@nestjs/common';

//Prisma
import { Company, Prisma } from '@Types';

//Generic
import { PrismaService } from '@/infrastructure/prisma/prisma.service';
import { GenericService } from '@Application/services/generic.service';

@Injectable()
export class CompanyService extends GenericService<
    Company,
    Prisma.CompanyDelegate
> {
    constructor(protected readonly prismaService: PrismaService) {
        const schema = 'company';
        super(schema, prismaService);
    }

    //Find User Roles
    // async findUserCompanies(userId: number): Promise<Company[]> {
    //     const elements = await this.prismaService.company.findMany({
    //         where: {
    //             userId,
    //         },
    //     });
    //     return elements;
    // }
}
