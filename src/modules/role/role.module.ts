import { Module } from '@nestjs/common';

//Controller
import { RoleController } from './role.controller';

//Service
import { RoleService } from './role.service';

@Module({
    exports: [RoleService],
    controllers: [RoleController],
    providers: [RoleService],
})
export class RoleModule {}
