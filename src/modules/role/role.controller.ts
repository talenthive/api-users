import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
} from '@nestjs/common';

//Services
import { RoleService } from './role.service';

//DTO
import { RoleCreateInput } from './dto/role.create.input';
import { RoleUpdateInput } from './dto/role.update.input';

//Prisma
import { Prisma, Role } from '@Types';

//Guards
import { CompanyFilter } from '@/application/filters/company.filter';
import { Permission } from '@/application/guards/permission.guard';
import { CheckRole } from '@/application/guards/role.guard';
import { HydrateEntity } from '@/application/pipes/hydrate-entity.pipe';

//Pipes
@Controller('role')
export class RoleController {
    constructor(private readonly roleService: RoleService) {}

    //Create Role
    @Post('/create')
    @Permission('ROLE', 'CREATE')
    async create(@Body() body: RoleCreateInput): Promise<Role> {
        return await this.roleService.create(body);
    }

    //Get Roles
    @Get('/find')
    @Permission('ROLE', 'READ')
    @CompanyFilter()
    async find(@Body() query: Prisma.RoleAggregateArgs): Promise<Role[]> {
        return this.roleService.find(query);
    }

    //Get Role
    @Get('/find/:roleId')
    @Permission('ROLE', 'READ')
    @CompanyFilter()
    @CheckRole(true)
    async findOne(
        @Param('roleId')
        roleId: number
    ): Promise<Role> {
        return this.roleService.findById(roleId);
    }

    //Update Role
    @Patch('/update/:roleId')
    @Permission('ROLE', 'UPDATE')
    @CheckRole(true)
    async update(
        @HydrateEntity('role') role: Role,
        @Body()
        body: RoleUpdateInput
    ): Promise<Role> {
        const updatedRole = await this.roleService.update(role.id, body);
        return updatedRole;
    }

    //Delete Role
    @Delete('/delete/:roleId')
    @Permission('ROLE', 'DELETE')
    @CheckRole(true)
    async delete(@HydrateEntity('role') role: Role): Promise<Boolean> {
        return this.roleService.delete(role.id);
    }
}
