import { Injectable } from '@nestjs/common';

//Prisma
import { Prisma, Role } from '@Types';

//Generic
import { PrismaService } from '@/infrastructure/prisma/prisma.service';
import { GenericService } from '@Application/services/generic.service';

@Injectable()
export class RoleService extends GenericService<Role, Prisma.RoleDelegate> {
    constructor(protected readonly prismaService: PrismaService) {
        const schema = 'role';
        super(schema, prismaService);
    }
}
