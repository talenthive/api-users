import { CreateDTO } from '@/application/dtos/create.dto';
import { EntityExists } from '@/application/validation/entity.validation';
import { Prisma } from '@Types';
import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class RoleCreateInput
    extends CreateDTO
    implements Prisma.RoleCreateInput
{
    @IsString()
    @IsNotEmpty()
    name: string;

    @IsString()
    publicId?: string;

    @IsNumber()
    @IsOptional()
    @EntityExists()
    companyId?: number;
}
