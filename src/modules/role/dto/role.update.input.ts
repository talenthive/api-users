import { PartialType } from '@nestjs/mapped-types';
import { RoleCreateInput } from './role.create.input';

export class RoleUpdateInput extends PartialType(RoleCreateInput) {}
