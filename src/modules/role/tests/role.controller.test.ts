import { Test } from '@nestjs/testing';
import { RoleController } from '../role.controller';
import { RoleService } from '../role.service';
import { TestModule } from '@/test.module';

describe('RoleController', () => {
    let roleController: RoleController;

    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [TestModule],
            controllers: [RoleController],
            providers: [RoleService],
        }).compile();

        roleController = moduleRef.get<RoleController>(RoleController);
    });

    it('Role Controller Should be defined', () => {
        expect(roleController).toBeDefined();
    });
});
