import { Test, TestingModule } from '@nestjs/testing';

import { TestModule } from '@/test.module';
import { createFakeRole } from '@Infrastructure/faker/role.faker';
import { RoleCreateInput } from '../dto/role.create.input';
import { RoleService } from '../role.service';

describe('Role Service', () => {
    let roleService: RoleService;
    let roleInput: RoleCreateInput;
    let roleId: number;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [TestModule],
            controllers: [],
            providers: [RoleService],
        }).compile();

        roleService = module.get<RoleService>(RoleService);
    });

    //Service
    it('Role Service should be defined', () => {
        expect(roleService).toBeDefined();
    });

    //Generate fake role data
    it('Should generate a fake role', async () => {
        roleInput = await createFakeRole();
        expect(roleInput).toBeDefined();
        expect(Object.keys(roleInput).length).toBeGreaterThan(0);
    });

    //Create Role
    it('Should create a role', async () => {
        const newRole = await roleService.create(roleInput);
        roleId = newRole.id;
        expect(Object.keys(newRole).length).toBeGreaterThan(0);
    });

    //Update Role
    it('Should update a role', async () => {
        const fakeRole = createFakeRole();
        const updatedRole = { ...fakeRole, id: roleId };
        const role = await roleService.update(roleId, updatedRole);
        expect(Object.keys(role).length).toBeGreaterThan(0);
    });

    //Find Roles
    it('Should return roles', async () => {
        const roles = await roleService.find({});
        expect(roles?.length).toBeGreaterThan(0);
    });

    //Delete Role
    it('Should delete the role', async () => {
        const result = await roleService.delete(roleId);
        expect(result).toBeTruthy();
    });
});
