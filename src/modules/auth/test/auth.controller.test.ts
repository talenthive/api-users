import { PassportModule } from '@nestjs/passport';
import { Test } from '@nestjs/testing';

//Auth
import { AuthController } from '../auth.controller';
import { AuthService } from '../auth.service';
import { JwtStrategy } from '../strategies/auth.jwt.strategy';

//Modules
import { createFakeUser } from '@/infrastructure/faker/user.faker';
import { UserCreateInput } from '@/modules/user/dto/user.create.input';
import { UserModule } from '@/modules/user/user.module';
import { UserService } from '@/modules/user/user.service';
import { TestModule } from '@/test.module';
import { App, User } from '@Types';

//Prisma Connect
import prisma from '@Prisma/prisma.connect';

describe('AuthController', () => {
    let authController: AuthController;

    let service: AuthService;
    let userService: UserService;

    let generatedUser: UserCreateInput;
    let createdUser: User;
    let app: App;

    beforeAll(async () => {
        const module = await Test.createTestingModule({
            imports: [TestModule, UserModule, PassportModule],
            controllers: [AuthController],
            providers: [AuthService, JwtStrategy],
        }).compile();

        authController = module.get<AuthController>(AuthController);

        //Services
        userService = module.get<UserService>(UserService);

        //Generate User
        generatedUser = createFakeUser();
        createdUser = await userService.create(generatedUser);

        //Last App
        app = await prisma.app.findFirst();
    });

    it('Auth Controller Should be defined', () => {
        expect(authController).toBeDefined();
    });

    it('Should be able to login', async () => {
        const login = await authController.login({
            app: app.id,
            email: generatedUser.email,
            password: generatedUser.password,
        });
        expect(login).toBeDefined();
        expect(typeof login.token).toBe('string');
    });
});
