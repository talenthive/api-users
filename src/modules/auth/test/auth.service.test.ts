//NestJs
import { Test, TestingModule } from '@nestjs/testing';
import * as bcrypt from 'bcryptjs';

//Auth
import { AuthService } from '../auth.service';

//User
import { createFakeUser } from '@/infrastructure/faker/user.faker';
import { UserCreateInput } from '@/modules/user/dto/user.create.input';
import { UserModule } from '@/modules/user/user.module';
import { UserService } from '@/modules/user/user.service';
import { TestModule } from '@/test.module';
import { App, User } from '@Types';

//Prisma Connect
import prisma from '@Prisma/prisma.connect';

describe('AuthService', () => {
    let service: AuthService;
    let userService: UserService;

    let generatedUser: UserCreateInput;
    let createdUser: User;
    let app: App;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [TestModule, UserModule],
            providers: [UserService, AuthService],
        }).compile();

        //Services
        service = module.get<AuthService>(AuthService);
        userService = module.get<UserService>(UserService);

        //Generate User
        generatedUser = createFakeUser();
        createdUser = await userService.create(generatedUser);

        //Last App
        app = await prisma.app.findFirst();
    });

    //After All
    afterAll(async () => {
        //Delete User
        await userService.delete(createdUser.id);
    });

    it('Service should be defined', () => {
        expect(service).toBeDefined();
    });

    it('Should validate the password correctly', async () => {
        const password = generatedUser.password;
        const hashedPassword = await bcrypt.hash(password, 10);
        const isValid = await service.validatePassword(
            password,
            hashedPassword
        );
        expect(isValid).toBe(true);
    });

    it('Should validate the password correctly with the password from the database', async () => {
        const password = generatedUser.password;
        const hashedPassword = createdUser.password;
        const isValid = await service.validatePassword(
            password,
            hashedPassword
        );
        expect(isValid).toBe(true);
    });

    it('Should return false for an invalid password', async () => {
        const password = `${generatedUser.password}_WrongPassword`;
        const hashedPassword = await bcrypt.hash('plainPassword', 10);
        const isValid = await service.validatePassword(
            password,
            hashedPassword
        );
        expect(isValid).toBe(false);
    });

    it('Should be able to login', async () => {
        const login = await service.login({
            app: app.id,
            email: generatedUser.email,
            password: generatedUser.password,
        });
        expect(login).toBeDefined();
        expect(typeof login.token).toBe('string');
    });
});
