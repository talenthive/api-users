import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';

//Auth
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

//Strategies
import { JwtStrategy } from './strategies/auth.jwt.strategy';

import { UserModule } from '../user/user.module';

@Module({
    imports: [UserModule, PassportModule],
    controllers: [AuthController],
    providers: [AuthService, JwtStrategy],
})
export class AuthModule {}
