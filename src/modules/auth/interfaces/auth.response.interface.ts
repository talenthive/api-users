import { User } from '@Types';

export interface AuthResponse {
    token: string;
    credentials: User;
}
