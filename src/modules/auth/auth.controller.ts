import { Body, Controller, Get, Post } from '@nestjs/common';

//DTO
import { AuthDetailsdDto } from './dto/auth.dto';

//Services
import { AuthService } from './auth.service';

//Interface
import { AuthResponse } from './interfaces/auth.response.interface';

//Decorators
import { AuthNotRequired } from '@/application/guards/auth.guard';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    //Login
    @Post('login')
    @AuthNotRequired(true)
    async login(@Body() AuthDetails: AuthDetailsdDto): Promise<AuthResponse> {
        return await this.authService.login(AuthDetails);
    }

    //Check Token
    @Get('/validate')
    async validate() {
        return true;
    }
}
