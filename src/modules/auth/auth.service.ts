import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';

//BCrypt
import * as bcrypt from 'bcryptjs';

//Service
import { UserService } from '@Modules/user/user.service';
import { LambdaService } from '@/infrastructure/lambda/lambda.service';
import { RedisService } from '@/infrastructure/redis/redis.service';

//DTO
import { AuthDetailsdDto } from './dto/auth.dto';

//Interfaces
import { AuthPayload } from './interfaces/auth.payload.interface';
import { AuthResponse } from './interfaces/auth.response.interface';

//Errors
import { ConfigService } from '@nestjs/config';
import { RedisExperiyDurationEnum } from '@/infrastructure/redis/enums/redis.expiry-duration.enums';

@Injectable()
export class AuthService {
    constructor(
        private readonly userService: UserService,
        private readonly configService: ConfigService,
        private readonly lambaService: LambdaService,
        protected readonly RedisService?: RedisService
    ) {}

    //Check Password
    async validatePassword(
        password: string,
        hashedPassword: string
    ): Promise<boolean> {
        const passwordHash = this.configService.get<string>('PASSWORD_HASH');
        if (!passwordHash) {
            throw new Error('JWT Secret not set in environment variables');
        }
        return await bcrypt.compare(password, hashedPassword);
    }

    //Login
    async login(data: AuthDetailsdDto): Promise<AuthResponse> {
        try {
            const { app, email } = data;

            //1 - Let's check if the user exists
            const credentials = await this.userService.findOneByEmail(email);

            if (!credentials) {
                throw new Error();
            }

            //4 - Build the Payload
            const payload: AuthPayload = {
                appId: app,
                userId: credentials.id,
            };

            //Generate token from Lambda
            const response = await this.lambaService.invokeApi({
                url: `${process.env.API_TOKEN_URL}/token/generate`,
                method: 'POST',
                payload,
            });
            const token = response.data;

            //Cache Token
            const key = `${credentials.id.toString()}:${app}`;
            await this.RedisService.setWithExpiry(
                'token',
                key,
                token,
                RedisExperiyDurationEnum.ONE_HOUR
            );

            return {
                token: token,
                credentials: credentials,
            };
        } catch (e) {
            throw new HttpException(
                'Your Email or Password is incorrect.',
                HttpStatus.FORBIDDEN
            );
        }
    }

    //Validate Token
    async validateToken(token: string): Promise<any> {
        try {
            return token;
        } catch (e) {
            throw new Error('Cannot verify token');
        }
    }
}
