import { EntityExists } from '@/application/validation/entity.validation';
import { Transform } from 'class-transformer';
import {
    IsEmail,
    IsString,
    IsNotEmpty,
    MinLength,
    IsNumber,
} from 'class-validator';

export class LostPasswordDto {
    @IsNotEmpty()
    @IsEmail()
    email: string;
}
export class ChangePasswordDto {
    @IsString()
    @IsNotEmpty()
    @MinLength(8)
    password: string;
}
export class AuthDetailsdDto {
    @EntityExists({ context: 'app' })
    @Transform(({ value }) =>
        typeof value === 'string' ? parseInt(value) : value
    )
    @IsNumber()
    app: number;

    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    password: string;
}
