import { PermissionOrigin } from '@/application/enums/permission.origin.enum';
import { EntityExists } from '@/application/validation/entity.validation';
import { $Enums } from '@Types';
import { Transform } from 'class-transformer';
import { IsEnum, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';

export class PermissionUserArgs {
    @IsOptional()
    @EntityExists({ context: 'app' })
    @Transform(({ value }) =>
        typeof value === 'string' ? parseInt(value) : value
    )
    @IsNumber()
    app?: number;
}
