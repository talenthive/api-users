import { EntityExists } from '@/application/validation/entity.validation';
import { $Enums } from '@Types';
import { Type } from 'class-transformer';
import {
    IsArray,
    IsEnum,
    IsNotEmpty,
    IsNumber,
    IsObject,
    IsOptional,
    ValidateNested,
} from 'class-validator';

export class PermissionUpdateList {
    @IsEnum($Enums.Entity)
    @IsNotEmpty()
    entity: $Enums.Entity;

    @IsEnum($Enums.PermissionAction)
    @IsNotEmpty()
    action: $Enums.PermissionAction;
}

export class PermissionUpdateInput {
    @IsNumber()
    @IsNotEmpty()
    @EntityExists({ context: 'app' })
    appId: number;

    @IsNumber()
    @IsOptional()
    @EntityExists()
    roleId?: number;

    @IsNumber()
    @IsOptional()
    @EntityExists()
    companyId?: number;

    @IsNumber()
    @IsOptional()
    userId?: number;

    @IsArray()
    @ValidateNested()
    @Type(() => PermissionUpdateList)
    permissions: PermissionUpdateList[];
}
