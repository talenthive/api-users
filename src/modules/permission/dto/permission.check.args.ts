import { $Enums } from '@Types';
import { IsEnum } from 'class-validator';

export class PermissionCheckArgs {
    @IsEnum($Enums.Entity)
    entity?: $Enums.Entity;

    @IsEnum($Enums.PermissionAction)
    action?: $Enums.PermissionAction;
}
