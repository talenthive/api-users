import { CreateDTO } from '@/application/dtos/create.dto';
import { EntityExists } from '@/application/validation/entity.validation';
import { $Enums, Prisma } from '@Types';
import { IsEnum, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';

export class PermissionCreateInput
    extends CreateDTO
    implements Prisma.PermissionUncheckedCreateInput
{
    @IsEnum($Enums.Entity)
    @IsNotEmpty()
    entity: $Enums.Entity;

    @IsEnum($Enums.PermissionAction)
    @IsNotEmpty()
    action: $Enums.PermissionAction;

    @IsNumber()
    @IsNotEmpty()
    @EntityExists({ context: 'app' })
    appId: number;

    @IsNumber()
    @IsOptional()
    @EntityExists()
    roleId?: number;

    @IsNumber()
    @IsOptional()
    @EntityExists()
    companyId?: number;

    @IsNumber()
    @IsOptional()
    userId?: number;
}
