import { Body, Controller, Get, Param, Post, Query, Req } from '@nestjs/common';

//Services
import { PermissionService } from './permission.service';

//DTO
import { PermissionCheckArgs } from './dto/permission.check.args';
import { PermissionUpdateInput } from './dto/permission.update.input';
import { PermissionUserArgs } from './dto/permission.user.args';

//Prisma

//Pipes
import { HydrateUser } from '../user/user.hydrate';

//Interfaces
import { CheckCompany } from '@/application/guards/company.guard';
import { OnlyMe } from '@/application/guards/onlyMe.guard';
import { Permission } from '@/application/guards/permission.guard';
import { CheckRole } from '@/application/guards/role.guard';
import { UserWithAllRelations } from '../user/interfaces/user.all-relations.interface';
import { AppPermission } from './interfaces/permissions-by-user.interface';

@Controller('permission')
export class PermissionController {
    constructor(private readonly permissionService: PermissionService) {}

    //Update Permissions
    @Post('/update')
    @Permission('PERMISSION', 'UPDATE')
    async updatePermissions(
        @Body() permissions: PermissionUpdateInput
    ): Promise<Boolean> {
        return this.permissionService.updatePermissions(permissions);
    }

    //Get User App Permissions
    @Get('/user/:userId')
    @Permission('PERMISSION', 'READ')
    async findUserPermissions(
        @Param('userId')
        userId: number,
        @Query() query: PermissionUserArgs
    ): Promise<AppPermission[]> {
        return this.permissionService.findAppPermissions(query.app, {
            userIds: [userId],
        });
    }

    //Get Company App Permissions
    @Get('/company/:companyId')
    @Permission('PERMISSION', 'READ')
    @CheckCompany(true)
    async findCompanyPermissions(
        @Param('companyId')
        companyId: number,
        @Query() query: PermissionUserArgs
    ): Promise<AppPermission[]> {
        return this.permissionService.findAppPermissions(query.app, {
            companyIds: [companyId],
        });
    }

    //Get Role App Permissions
    @Get('/role/:roleId')
    @Permission('PERMISSION', 'READ')
    @CheckRole(true)
    async findRolePermissions(
        @Param('roleId')
        roleId: number,
        @Query() query: PermissionUserArgs
    ): Promise<AppPermission[]> {
        return this.permissionService.findAppPermissions(query.app, {
            roleIds: [roleId],
        });
    }

    //Get User App All Permissions
    @Get('/all/:userId')
    @Permission('PERMISSION', 'READ')
    @OnlyMe(true)
    async findUserAllPermissions(
        @Param('userId', HydrateUser)
        user: UserWithAllRelations,
        @Query() query: PermissionUserArgs
    ): Promise<AppPermission[]> {
        return this.permissionService.findUserPermissions(user, query.app);
    }

    //Check User App Permission
    @Get('/validate/:userId')
    @OnlyMe(true)
    async validateUserPermission(
        @Req() req: any,
        @Param('userId', HydrateUser)
        user: UserWithAllRelations,
        @Query() query: PermissionCheckArgs
    ): Promise<Boolean> {
        return this.permissionService.validateUserPermission(
            user,
            req.appId,
            query
        );
    }
}
