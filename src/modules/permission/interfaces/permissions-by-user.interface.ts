import { PermissionOrigin } from '@Enums/permission.origin.enum';
import { $Enums, Entity } from '@Types';

export interface AppPermission {
    entity: Entity;
    action: $Enums.PermissionAction;
    origin: PermissionOrigin;
}
