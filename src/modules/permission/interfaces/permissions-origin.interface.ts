export interface PermissionsOrigin {
    roleIds?: number[];
    companyIds?: number[];
    userIds?: number[];
}
