import { Injectable } from '@nestjs/common';

//Prisma
import { Company, Permission, Prisma, User } from '@Types';

//Services
import { PrismaService } from '@/infrastructure/prisma/prisma.service';
import { GenericService } from '@Application/services/generic.service';

//DTO
import { PermissionUpdateInput } from './dto/permission.update.input';

//Enums
import { PermissionOrigin } from '@Enums/permission.origin.enum';

//Interfaces
import { RedisExperiyDurationEnum } from '@/infrastructure/redis/enums/redis.expiry-duration.enums';
import { RedisService } from '@/infrastructure/redis/redis.service';
import { UserService } from '../user/user.service';
import { PermissionCheckArgs } from './dto/permission.check.args';
import { PermissionCreateInput } from './dto/permission.create.input';
import { AppPermission } from './interfaces/permissions-by-user.interface';
import { PermissionsOrigin } from './interfaces/permissions-origin.interface';

@Injectable()
export class PermissionService extends GenericService<
    Permission,
    Prisma.PermissionDelegate
> {
    constructor(
        protected readonly prismaService: PrismaService,
        protected readonly userService: UserService,
        protected readonly redisService?: RedisService
    ) {
        super('permission', prismaService);
    }

    buildWhereCondition(
        input: PermissionUpdateInput
    ): Prisma.PermissionWhereInput {
        const { appId, roleId, companyId, userId, permissions } = input;
        let condition = [];
        if (roleId) condition.push({ roleId: { in: [roleId] } });
        if (companyId) condition.push({ companyId: { in: [companyId] } });
        if (userId) condition.push({ userId: { in: [userId] } });
        return {
            appId: appId,
            AND: [{ OR: condition }, { OR: permissions }],
        };
    }

    //Update Permissions
    async updatePermissions(input: PermissionUpdateInput): Promise<Boolean> {
        const { appId, roleId, companyId, userId } = input;

        const whereConditions = this.buildWhereCondition(input);

        //Make all the permissions inactive
        await this.repository.updateMany({
            where: whereConditions,
            data: {
                active: false,
            },
        });

        //Find Permissions To Update
        let permissionsToUpdateIds: number[] = [];
        const permissionsToUpdate = await this.repository.findMany({
            where: {
                appId: appId,
                ...whereConditions,
            },
        });

        if (permissionsToUpdate.length > 0) {
            permissionsToUpdateIds = permissionsToUpdate.map((p) => p.id);
        }

        //Make the permissions to update active
        await this.repository.updateMany({
            where: {
                id: { in: permissionsToUpdateIds },
            },
            data: {
                active: true,
            },
        });

        //Find The permissions to create
        const setOfActionsAndEntities = new Set(
            permissionsToUpdate.map((item) => `${item.entity}-${item.action}`)
        );
        const permissionsToCreate = input.permissions.filter(
            (item) =>
                !setOfActionsAndEntities.has(`${item.entity}-${item.action}`)
        );
        const newPermissions: PermissionCreateInput[] = permissionsToCreate.map(
            (p) => {
                return {
                    appId,
                    userId,
                    companyId,
                    roleId,
                    ...p,
                };
            }
        );
        //Creation Process
        await this.repository.createMany({
            data: newPermissions,
        });

        return true;
    }

    //Validate Permission
    async validateUserPermission(
        user: User & { companies: Array<Company> },
        appId: number,
        query: PermissionCheckArgs
    ): Promise<Boolean> {
        const permissions = await this.findUserPermissions(user, appId);
        //Filter Permissions
        const filteredPermissions = permissions.filter(
            (p) => p.action === query.action && p.entity === query.entity
        );
        if (filteredPermissions.length === 0) return false;
        return true;
    }

    //Find App Permissions
    async findAppPermissions(
        appId: number,
        permissionOrigins: PermissionsOrigin
    ): Promise<AppPermission[]> {
        const foundPermissions = await this.repository.findMany({
            where: {
                appId,
                OR: [
                    { roleId: { in: permissionOrigins.roleIds } },
                    { companyId: { in: permissionOrigins.companyIds } },
                    { userId: { in: permissionOrigins.userIds } },
                ],
            },
        });
        const permissions = this.transformPermissions(foundPermissions);
        return permissions;
    }

    //Find User Permissions
    async findUserPermissions(
        user: User & { companies: Array<Company> },
        appId: number
    ): Promise<AppPermission[]> {
        let permissions: AppPermission[];
        const userId = user.id;
        const key = `${userId}:${appId}`;

        //Check Permissions in cache
        permissions = JSON.parse(
            await this.redisService.get('permissions', key)
        );

        //Cach Not Found
        if (!permissions) {
            //Find Companies of the User
            const companies = user.companies;
            const companiesId =
                companies.length > 0 ? companies.map((r) => r.id) : [];

            //Find Roles Of The User
            const roles = await this.userService.findUserRoles(user.id);
            const rolesId = roles.map((r) => r.id);

            const foundPermissions = await this.repository.findMany({
                where: {
                    appId,
                    OR: [
                        { roleId: { in: rolesId } },
                        { companyId: { in: companiesId } },
                        { userId: user.id },
                    ],
                },
            });

            //Transform Data
            permissions = this.transformPermissions(foundPermissions);

            //Cache Permissions
            await this.redisService.setWithExpiry(
                'permissions',
                key,
                JSON.stringify(permissions),
                RedisExperiyDurationEnum.ONE_HOUR
            );
        }
        return permissions;
    }

    //Transform Data
    transformPermissions(permissions: Permission[]): AppPermission[] {
        const listPermissions = [] as AppPermission[];
        if (permissions.length === 0) return [];

        permissions.map((p) => {
            //Origin
            let origin = PermissionOrigin.USER;
            if (p.companyId) origin = PermissionOrigin.COMPANY;
            else if (p.roleId) origin = PermissionOrigin.ROLE;

            listPermissions.push({
                entity: p.entity,
                action: p.action,
                origin,
            });
        });

        return listPermissions;
    }
}
