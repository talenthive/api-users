import { Test, TestingModule } from '@nestjs/testing';

import { RoleModule } from '@/modules/role/role.module';
import { RoleService } from '@/modules/role/role.service';
import { UserWithAllRelations } from '@/modules/user/interfaces/user.all-relations.interface';
import { UserService } from '@/modules/user/user.service';
import { TestModule } from '@/test.module';
import {
    createFakePermission,
    generateFakePermissionActions,
} from '@Infrastructure/faker/permission.faker';
import { PermissionCreateInput } from '../dto/permission.create.input';
import {
    PermissionUpdateInput,
    PermissionUpdateList,
} from '../dto/permission.update.input';
import { PermissionService } from '../permission.service';

describe('Permission Service', () => {
    let userService: UserService;
    let permissionService: PermissionService;
    let permissionInput: PermissionCreateInput;
    let permissionActions: PermissionUpdateList[] = [];
    let user: UserWithAllRelations;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [TestModule, RoleModule],
            controllers: [],
            providers: [PermissionService, RoleService, UserService],
        }).compile();

        permissionService = module.get<PermissionService>(PermissionService);
        userService = module.get<UserService>(UserService);

        //Generate Data
        permissionInput = await createFakePermission();
        permissionActions.push({
            action: permissionInput.action,
            entity: permissionInput.entity,
        });

        if (permissionInput.userId) {
            user = await userService.findById(permissionInput.userId);
        }
    });

    //Service
    it('Permission Service should be defined', () => {
        expect(permissionService).toBeDefined();
    });

    //Generate fake permission data
    it('Should generate a fake permission', async () => {
        expect(permissionInput).toBeDefined();
    });

    //Generate fake permission actions
    it('Should generate fake permission actions', async () => {
        permissionActions.concat(await generateFakePermissionActions());
        expect(permissionActions).toBeDefined();
    });

    //Update Permissions
    it('Should update permissions', async () => {
        const permissionsInput: PermissionUpdateInput = {
            appId: permissionInput.appId,
            roleId: permissionInput?.roleId,
            companyId: permissionInput?.companyId,
            userId: permissionInput?.userId,
            permissions: permissionActions,
        };

        const result = await permissionService.updatePermissions(
            permissionsInput
        );

        expect(result).toBe(true);
    });

    //Find Permissions
    it('Should return permissions', async () => {
        const permissions = await permissionService.find({});
        expect(permissions?.length).toBeGreaterThan(0);
    });

    //Find User Permissions
    it('Should find user permissions', async () => {
        if (user) {
            const result = await permissionService.findUserPermissions(
                user,
                permissionInput.appId
            );
            expect(result.length).toBeGreaterThan(0);
        }
    });

    //Validate User Permission
    it('Validate User Permission', async () => {
        if (user) {
            const result = await permissionService.validateUserPermission(
                user,
                permissionInput.appId,
                {
                    action: permissionActions[0].action,
                    entity: permissionActions[0].entity,
                }
            );

            expect(result).toBe(true);
        }
    });
});
