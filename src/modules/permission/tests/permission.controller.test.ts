import {
    createFakePermission,
    generateFakePermissionActions,
} from '@/infrastructure/faker/permission.faker';
import { CompanyService } from '@/modules/company/company.service';
import { RoleModule } from '@/modules/role/role.module';
import { RoleService } from '@/modules/role/role.service';
import { UserWithAllRelations } from '@/modules/user/interfaces/user.all-relations.interface';
import { UserService } from '@/modules/user/user.service';
import { TestModule } from '@/test.module';
import { Test } from '@nestjs/testing';
import { Company, Role } from '@Types';
import { PermissionCreateInput } from '../dto/permission.create.input';
import {
    PermissionUpdateInput,
    PermissionUpdateList,
} from '../dto/permission.update.input';
import { PermissionController } from '../permission.controller';
import { PermissionService } from '../permission.service';

describe('PermissionController', () => {
    let permissionController: PermissionController;

    let userService: UserService;
    let companyService: CompanyService;
    let roleService: RoleService;

    let user: UserWithAllRelations;
    let company: Company;
    let role: Role;

    let permissionInput: PermissionCreateInput;
    let permissionActions: PermissionUpdateList[] = [];

    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [TestModule, RoleModule],
            controllers: [PermissionController],
            providers: [
                PermissionService,
                RoleService,
                UserService,
                CompanyService,
            ],
        }).compile();

        permissionController =
            moduleRef.get<PermissionController>(PermissionController);
        userService = moduleRef.get<UserService>(UserService);
        companyService = moduleRef.get<CompanyService>(CompanyService);
        roleService = moduleRef.get<RoleService>(RoleService);

        permissionInput = await createFakePermission();

        company = await companyService.findFirstElement();
        role = await roleService.findFirstElement();
        if (permissionInput.userId) {
            user = await userService.findById(permissionInput.userId);
        }
    });

    it('Permission Controller Should be defined', () => {
        expect(permissionController).toBeDefined();
    });

    //Generate fake permission data
    it('Should generate a fake permission', async () => {
        permissionInput = await createFakePermission();
        permissionActions.push({
            action: permissionInput.action,
            entity: permissionInput.entity,
        });
        expect(permissionInput).toBeDefined();
    });

    //Generate fake permission actions
    it('Should generate fake permission actions', async () => {
        permissionActions.concat(await generateFakePermissionActions());
        expect(permissionActions).toBeDefined();
    });

    //Update Permissions
    it('Should update permissions', async () => {
        const permissionsInput: PermissionUpdateInput = {
            appId: permissionInput.appId,
            roleId: permissionInput?.roleId,
            companyId: permissionInput?.companyId,
            userId: permissionInput?.userId,
            permissions: permissionActions,
        };

        const result = await permissionController.updatePermissions(
            permissionsInput
        );
        expect(result).toBe(true);
    });

    //Find User Permissions
    it('Should find all user permissions', async () => {
        if (user) {
            const result = await permissionController.findUserAllPermissions(
                user,
                {
                    app: permissionInput.appId,
                }
            );
            expect(result.length).toBeGreaterThan(0);
        }
    });

    //Validate User Permission
    it('Validate User Permission', async () => {
        if (user) {
            const result = await permissionController.validateUserPermission(
                { appId: permissionInput.appId },
                user,
                {
                    action: permissionActions[0].action,
                    entity: permissionActions[0].entity,
                }
            );
            expect(result).toBe(true);
        }
    });

    //Find Role App Permissions
    it('Should Find  Role permissions', async () => {
        const result = await permissionController.findRolePermissions(role.id, {
            app: permissionInput.appId,
        });
        expect(result.length).toBeDefined();
    });

    //Find Company App Permissions
    it('Should Company Permissions', async () => {
        const result = await permissionController.findCompanyPermissions(
            company.id,
            {
                app: permissionInput.appId,
            }
        );
        expect(result.length).toBeDefined();
    });

    //Find User App Permissions
    it('Should User permissions', async () => {
        if (user) {
            const result = await permissionController.findUserPermissions(
                user.id,
                {
                    app: permissionInput.appId,
                }
            );
            expect(result.length).toBeGreaterThan(0);
        }
    });

    //Find User App Permissions
    it('Should find all user permissions', async () => {
        if (user) {
            const result = await permissionController.findUserPermissions(
                user.id,
                {
                    app: permissionInput.appId,
                }
            );
            expect(result.length).toBeGreaterThan(0);
        }
    });
});
