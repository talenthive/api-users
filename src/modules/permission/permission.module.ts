import { forwardRef, Module } from '@nestjs/common';

//Controller
import { PermissionController } from './permission.controller';

//Service
import { RoleModule } from '../role/role.module';
import { UserModule } from '../user/user.module';
import { PermissionService } from './permission.service';

@Module({
    imports: [RoleModule, forwardRef(() => UserModule)],
    exports: [PermissionService],
    controllers: [PermissionController],
    providers: [PermissionService],
})
export class PermissionModule {}
