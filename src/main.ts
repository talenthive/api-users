//NestJs
import { NestFactory } from '@nestjs/core';
import { INestApplication, Logger, ValidationPipe } from '@nestjs/common';

//Express
import { ExpressAdapter } from '@nestjs/platform-express';
import * as express from 'express';

//AWS
import { Context, Handler } from 'aws-lambda';
import * as serverlessExpress from 'aws-serverless-express';

//Logger
import { WinstonModule } from 'nest-winston';
import { winstonConfig } from './infrastructure/winston/winston.config';

//Module
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

const expressApp = express();

function buildApp(app: INestApplication<any>) {
    //Validation Pipes
    app.useGlobalPipes(
        new ValidationPipe({
            transform: true,
        })
    );
}

async function bootstrap(expressInstance) {
    //Dev
    if (process.env.NODE_ENV === 'dev') {
        const app = await NestFactory.create(AppModule, {
            logger: WinstonModule.createLogger(winstonConfig),
        });
        const config = new DocumentBuilder()
            .setTitle('App Core Documentation')
            .setDescription('The App Core API description')
            .setVersion('1.0')
            .addTag('core')
            .build();
        const document = SwaggerModule.createDocument(app, config);
        SwaggerModule.setup('api', app, document);
        buildApp(app);
        await app.listen(process.env.PORT || 3000);
    }
    //Other environments
    else {
        const app = await NestFactory.create(
            AppModule,
            new ExpressAdapter(expressInstance),
            {
                logger: WinstonModule.createLogger(winstonConfig),
            }
        );
        buildApp(app);
        await app.init();
        return app;
    }
}

const server = serverlessExpress.createServer(expressApp);

export const handler: Handler = (event: any, context: Context) =>
    serverlessExpress.proxy(server, event, context);

bootstrap(expressApp)
    .then(() => Logger.log('Nest Is Ready'))
    .catch((err) => Logger.error('Nest broken', err));
