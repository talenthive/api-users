export const removeIdFromString = (input: string): string => {
    let count = 0;
    const toRemove = 'Id';

    // Find the last two occurrences of "Id" and remove them
    const updatedString = input
        .split('')
        .reverse()
        .join('')
        .replace(
            new RegExp(toRemove.split('').reverse().join(''), 'g'),
            (match, offset, string) => {
                if (count < 2) {
                    count++;
                    return '';
                }
                return match;
            }
        )
        .split('')
        .reverse()
        .join('');

    return updatedString;
};
