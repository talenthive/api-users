import { Injectable, PipeTransform } from '@nestjs/common';
import { isArray } from 'class-validator';

@Injectable()
export class BuildQuery implements PipeTransform {
    transform(value: any, metadata: any): any {
        const { companyId } = value;
        if (companyId && isArray(companyId)) {
            Object.assign(value, {
                companyId: { in: companyId },
            });
        }
        return value;
    }
}
