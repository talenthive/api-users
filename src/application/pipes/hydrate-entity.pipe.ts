import {
    createParamDecorator,
    ExecutionContext,
    HttpException,
    HttpStatus,
} from '@nestjs/common';

//Prisma Connect
import prisma from '@Prisma/prisma.connect';

export const HydrateEntity = createParamDecorator(
    async (schema: string, ctx: ExecutionContext) => {
        const request = ctx.switchToHttp().getRequest();
        const params = request.params;

        //Get Value Id
        let value = params?.id;
        typeof value === 'string' ? (value = parseInt(value)) : value;

        //User
        const user = request.user;
        const userCompanyIds = user?.companies.map((c) => c.id);

        //Get Entity
        const entity = await prisma[schema].findUnique({
            where: { id: value },
            include: {
                company: true,
            },
        });

        //If the entity is linked to a company, let's proceed to verifications
        if (!request.superAdmin && entity?.company) {
            //Check if the company of the entity belongs to the user
            if (
                userCompanyIds.length === 0 ||
                !userCompanyIds.include(entity.company.id)
            ) {
                throw new HttpException(
                    'Action Forbidden',
                    HttpStatus.FORBIDDEN
                );
            }
        }

        if (!entity) {
            throw new HttpException('Entity not found', HttpStatus.BAD_REQUEST);
        }
        return entity;
    }
);
