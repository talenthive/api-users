export enum PermissionOrigin {
    COMPANY = 'COMPANY',
    ROLE = 'ROLE',
    USER = 'USER',
}
