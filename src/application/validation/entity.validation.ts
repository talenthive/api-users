import prisma from '@/infrastructure/prisma/prisma.connect';
import { removeIdFromString } from '@Functions/remove-id-from-string.function';
import {
    registerDecorator,
    ValidationArguments,
    ValidationOptions,
    ValidatorConstraint,
    ValidatorConstraintInterface,
} from 'class-validator';

//Prisma

@ValidatorConstraint({ async: false })
export class EntityExistsConstraints implements ValidatorConstraintInterface {
    async validate(value: number, args: ValidationArguments) {
        const property = args.property;
        const entity = removeIdFromString(property);

        const repo = prisma[entity];

        const count = await repo.count({
            where: {
                id: typeof value === 'string' ? parseInt(value) : value,
            },
        });

        if (count === 0) return false;

        return true;
    }

    defaultMessage(args: ValidationArguments) {
        return 'Entity does not exist!';
    }
}

export function EntityExists(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: EntityExistsConstraints,
        });
    };
}
