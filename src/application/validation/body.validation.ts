import { UtilityService } from '@/infrastructure/utilities/utilities.service';
import { Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class ValidateBody implements PipeTransform {
    constructor(private readonly utilityService: UtilityService) {}

    async transform(value: any, metadata: any) {
        //Public Id
        if (value.name) {
            const publicId = await this.utilityService.generatePublicId(
                value.name,
                'company'
            );

            value.publicId = publicId;
        }

        return value;
    }
}
