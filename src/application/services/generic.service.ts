//NestJs
import { Injectable } from '@nestjs/common';

//Prisma
import { PrismaClient } from '@Types';

//Services
import { PrismaService } from '@Prisma/prisma.service';

interface SchemaProperties {
    id: number;
}

@Injectable()
export abstract class GenericService<
    Schema extends SchemaProperties,
    SchemaDelegate
> {
    protected prisma: PrismaClient;
    protected repository: SchemaDelegate;
    protected useCache: boolean = false;

    constructor(
        protected readonly schema: string,
        protected readonly prismaService: PrismaService
    ) {
        if (!schema) throw Error('Schemá is not defined');
        this.prisma = new PrismaClient();
        this.repository = this.prisma[schema];
    }

    //Create Element
    async create(data: any): Promise<Schema> {
        const element: Schema = await this.prisma[this.schema].create({
            data,
        });
        return element;
    }

    //Create Many Elements
    async createMany(data: any[]): Promise<Schema[]> {
        const elements: Schema[] = await this.prisma[this.schema].createMany({
            data,
        });
        return elements;
    }

    //Find Elements
    async find<T>(params: T): Promise<Schema[]> {
        const elements = await this.prismaService[this.schema].findMany({
            where: params,
        });
        return elements;
    }

    //Find Single Element
    async findById(id: number): Promise<Schema> {
        let element: Schema;

        element = <Schema>await this.prismaService[this.schema].findUnique({
            where: {
                id,
            },
        });

        return element;
    }

    //Find First Element
    async findFirstElement(): Promise<Schema> {
        const elements = <Schema[]>await this.prismaService[
            this.schema
        ].findMany({
            orderBy: {
                id: 'asc',
            },
            take: 1,
        });
        return elements[0] ? elements[0] : null;
    }

    //Find Last Element
    async findLastElement(): Promise<Schema> {
        const elements = <Schema[]>await this.prismaService[
            this.schema
        ].findMany({
            orderBy: {
                id: 'desc',
            },
            take: 1,
        });
        return elements[0] ? elements[0] : null;
    }

    //Find By Ids
    async findByIds(ids: string[]): Promise<Schema[]> {
        const elements = await this.prismaService[this.schema].findMany({
            where: { id: { in: ids } },
        });
        return elements;
    }

    //Update Element
    async update(id: number, body: any): Promise<Schema> {
        const existingElement = this.findById(id);
        const updatedElement = { ...existingElement, ...body };
        const result = <Schema>await this.prismaService[this.schema].update({
            where: {
                id,
            },
            data: updatedElement,
        });
        return result;
    }

    //Remove Element
    async delete(id: number): Promise<Boolean> {
        const result = await this.prismaService[this.schema].delete({
            where: {
                id,
            },
        });
        return true;
    }

    //Count Element
    async count(where: any): Promise<Number> {
        const result = await this.prismaService[this.schema].count({
            where,
        });
        return result;
    }
}
