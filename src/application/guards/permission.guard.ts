import { Injectable, ExecutionContext, HttpStatus } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';

//Entities

//Handle Error

//Services

import { SetMetadata } from '@nestjs/common';
import { PermissionService } from '@/modules/permission/permission.service';
import { $Enums } from '@Types';

type PermissionQueryArray = [$Enums.Entity, $Enums.PermissionAction];

export const Permission = (...args: any[]) =>
    SetMetadata('PermissionRestriction', args);

@Injectable()
export class PermissionGuard extends AuthGuard('jwt') {
    constructor(
        private reflector: Reflector,
        private readonly permissionService: PermissionService
    ) {
        super();
    }
    async canActivate(context: ExecutionContext): Promise<any> {
        const user = context.switchToHttp().getRequest().user;
        const superAdmin = context.switchToHttp().getRequest().superAdmin;
        const appId = context.switchToHttp().getRequest().appId;

        //User is a super admin
        if (superAdmin) return true;

        const restriction: PermissionQueryArray = this.reflector.get(
            'PermissionRestriction',
            context.getHandler()
        );

        //No Restriction
        if (!restriction) return true;

        const [entity, action] = restriction;

        //No Entity Or Action
        if (entity === null || action === null) return true;

        const result = await this.permissionService.validateUserPermission(
            user,
            appId,
            {
                entity: entity,
                action: action,
            }
        );

        return result;
    }

    // handleRequest(err: any, user: any, info: string) {
    //     return user;
    // }
}
