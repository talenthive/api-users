const path = require('path');
const slsw = require('serverless-webpack');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

module.exports = {
    context: __dirname,
    mode: slsw.lib.webpack.isLocal ? 'development' : 'production',
    devtool: 'source-map',
    entry: slsw.lib.entries,
    resolve: {
        extensions: ['.js', '.ts'],
        alias: {
            '@': path.resolve(__dirname, 'src'),
            '@Prisma': path.resolve(__dirname, 'prisma'),
            '@Application': path.resolve(__dirname, 'src/application'),
            '@Modules': path.resolve(__dirname, 'src/modules'),
            '@ModulesTools': path.resolve(__dirname, 'src/modulesTools'),
            '@Api': path.resolve(__dirname, 'src/api'),
            '@Mail': path.resolve(__dirname, 'src/api/mail'),
            '@Filters': path.resolve(__dirname, 'src/filters'),
            '@Pipes': path.resolve(__dirname, 'src/application/pipes'),
            '@Dtos': path.resolve(__dirname, 'src/application/dtos'),
            '@Infrastructure': path.resolve(__dirname, 'src/infrastructure'),
            '@Interceptors': path.resolve(
                __dirname,
                'src/application/interceptors'
            ),
            '@Schemas': path.resolve(__dirname, 'prisma/schema'),
            '@Functions': path.resolve(__dirname, 'src/application/functions'),
            '@common': path.resolve(__dirname, 'src/application'),
            '@Entities': path.resolve(__dirname, 'src/entities'),
            '@Helpers': path.resolve(__dirname, 'src/application/helpers'),
            '@Guards': path.resolve(__dirname, 'src/application/guards'),
            '@Decorators': path.resolve(
                __dirname,
                'src/application/decorators'
            ),
            '@Tools': path.resolve(__dirname, 'src/application/tools'),
        },
    },
    output: {
        libraryTarget: 'commonjs2',
        path: path.join(__dirname, '.webpack'),
        filename: '[name].js',
    },
    target: 'node',
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: [
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: true,
                        },
                    },
                ],
                exclude: /node_modules/,
            },
        ],
    },
    plugins: [new ForkTsCheckerWebpackPlugin()],
};
